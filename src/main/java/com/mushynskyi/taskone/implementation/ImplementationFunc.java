package com.mushynskyi.taskone.implementation;

import com.mushynskyi.taskone.Functional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ImplementationFunc {

  private static Logger logger = LogManager.getLogger(ImplementationFunc.class);

  public void printTaskOne(){
    Functional firstLambda = (a, b, c) -> {
      if(a>b)
        if(a>c)
          return a;
        else return c;
      if(a<b)
        if(b<c)
          return c;
        else
          return b;
      if(a<c)
        return c;
      return a;
    };
    logger.info(firstLambda.countvalues(3,2,1));

    Functional secondLambda = (a,b,c) -> {
      return (a+b+c)/3;
    };
    logger.info(secondLambda.countvalues(3,3,10));
  }
}
