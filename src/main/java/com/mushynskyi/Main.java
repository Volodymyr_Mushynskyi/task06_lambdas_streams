package com.mushynskyi;

import com.mushynskyi.menu.View;

public class Main {
  public static void main(String[] args) {
    new View().showMenu();
  }
}
