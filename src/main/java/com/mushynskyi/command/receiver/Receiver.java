package com.mushynskyi.command.receiver;

public class Receiver {

  public void actionOne(String commandField) {
    System.out.println("Instance -" + commandField);
  }

  public void actionTwo(String commandField) {
    System.out.println("Lambda -" + commandField);
  }

  public void actionThree(String commandField) {
    System.out.println("Anonymous -" + commandField);
  }

  public void actionFour(String commandField) {
    System.out.println("Reference -" + commandField);
  }
}
