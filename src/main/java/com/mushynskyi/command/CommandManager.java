package com.mushynskyi.command;

import com.mushynskyi.command.implementation.FirstCommand;
import com.mushynskyi.command.implementation.FourCommand;
import com.mushynskyi.command.implementation.SecondCommand;
import com.mushynskyi.command.implementation.ThirdCommand;
import com.mushynskyi.command.invoker.Invoker;
import com.mushynskyi.command.receiver.Receiver;

public class CommandManager {

  public void taskTwo(String command, String name) {

    Receiver receiver = new Receiver();

    Command commandOne = new FirstCommand(receiver, name);
    Command commandTwo = new SecondCommand(receiver, name);
    Command commandThree = new ThirdCommand(receiver, name);
    Command commandFour = new FourCommand(receiver, name);

    Invoker invoker = new Invoker();

    if (command.equals("firstCommand")) {
      invoker.executeObject(commandOne, name);
    }
    if (command.equals("secondCommand")) {
      invoker.executeLambda(commandTwo, name);
    }
    if (command.equals("thirdCommand")) {
      invoker.executeAnonymous(commandThree, receiver, name);
    }
    if (command.equals("fourCommand")) {
      invoker.executeMethod(commandFour, receiver, name);
    }
  }

}
