package com.mushynskyi.command.implementation;

import com.mushynskyi.command.Command;
import com.mushynskyi.command.receiver.Receiver;

public class SecondCommand implements Command {
  private Receiver receiver;
  private String commandField;

  public SecondCommand(Receiver receiver, String commandField) {
    this.receiver = receiver;
    this.commandField = commandField;
  }

  @Override
  public void execute(String name) {
    receiver.actionTwo(commandField);
  }
}
