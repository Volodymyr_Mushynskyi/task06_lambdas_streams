package com.mushynskyi.command.implementation;

import com.mushynskyi.command.Command;
import com.mushynskyi.command.receiver.Receiver;

public class FourCommand implements Command {
  private Receiver receiver;
  private String commandField;

  public FourCommand(Receiver receiver, String commandField) {
    this.receiver = receiver;
    this.commandField = commandField;
  }

  @Override
  public void execute(String name) {
    receiver.actionFour(commandField);
  }
}
