package com.mushynskyi.command;

public interface Command {
  void execute(String name);
}
