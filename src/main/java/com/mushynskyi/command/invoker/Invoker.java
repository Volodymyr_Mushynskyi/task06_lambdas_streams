package com.mushynskyi.command.invoker;

import com.mushynskyi.command.Command;
import com.mushynskyi.command.receiver.Receiver;

public class Invoker implements Command {

  private Command command;
  private Receiver receiver;

  private void setCommand(Command command) {
    this.command = command;
  }

  public void executeObject(Command command, String commandField) {
    setCommand(command);
    this.command.execute(commandField);
  }

  public void executeMethod(Command command, Receiver receiver, String commandField) {
    setCommand(command);
    this.command = this::execute;
  }

  public void executeLambda(Command command, String commandField) {
    setCommand(command);
    this.command = (name) -> {
      System.out.println("Lambda -" + commandField);
    };
    this.command.execute(commandField);
  }

  public void executeAnonymous(Command command, Receiver receiver, String commandField) {
    setCommand(command);
    this.command = new Command() {
      @Override
      public void execute(String name) {
        receiver.actionThree(name);
      }
    };
    this.command.execute(commandField);
  }

  @Override
  public void execute(String name) {
    receiver.actionFour(name);
  }
}
