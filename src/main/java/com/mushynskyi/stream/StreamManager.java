package com.mushynskyi.stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class StreamManager {

  private static Logger logger = LogManager.getLogger(StreamManager.class);
  private List<String> list;
  private Map<String, Long> collect;

  private List<Integer> createList() {
    List<Integer> list = new ArrayList<>();
    Random rand = new Random();
    Integer pick;

    for (int j = 0; j < 10; j++) {
      pick = rand.nextInt(100);
      list.add(pick);
    }
    logger.info("list :" + list);
    return list;
  }

  private void countStatistic() {
    IntSummaryStatistics statistics = createList().stream()
            .mapToInt(Integer::intValue)
            .summaryStatistics();
    logger.info("Statistic :" + statistics);
  }

  private void countStatisticByReduce() {
    List<Integer> integerList = createList();
    int sum = integerList.stream()
            .mapToInt(Integer::intValue)
            .sum();
    logger.info("Sum :" + sum);
    sum = integerList.stream()
            .reduce(0, Integer::sum);
    logger.info("Sum by reduce :" + sum);
  }

  public void taskThree() {
    countStatistic();
    countStatisticByReduce();
  }

  private void countNumberUniqueWords(String string) {
    list = Arrays.asList(string.split(" "));
    long count = list.stream()
            .distinct()
            .count();
    logger.info("Number of unique words :" + count);
  }

  private void sortUniqueWords() {
    logger.info("Sorted unique words :");
    list.stream()
            .distinct()
            .sorted()
            .collect(Collectors.toList())
            .forEach(System.out::println);
  }

  private void countWords() {
    logger.info("Count number of each word in the text :");
    collect = list.stream()
            .collect(groupingBy(Function.identity(), counting()));
    collect.entrySet().stream()
            .forEach(System.out::println);
  }

  private void countNumberEachSymbols(String string) {
    List<String> stringList = new ArrayList<>();
    String[] strings = string.split("");
    String temp = Arrays.toString(strings);
    char[] chars = temp.toCharArray();
    String[] lowerCaseString = new String[chars.length];
    for (int i = 0; i < chars.length; i++) {
      if (Character.isLowerCase(chars[i])) {
        lowerCaseString[i] = String.valueOf(chars[i]);
      } else {
        lowerCaseString[i] = "";
      }
    }
    logger.info("Print each symbols except upper case characters");
    stringList = Arrays.stream(lowerCaseString)
            .collect(Collectors.toList());
    collect = stringList.stream()
            .collect(groupingBy(Function.identity(), counting()));
    collect.entrySet().stream()
            .forEach(System.out::println);
  }

  public void taskFour(String string) {
    countNumberUniqueWords(string);
    sortUniqueWords();
    countWords();
    countNumberEachSymbols(string);
  }
}
