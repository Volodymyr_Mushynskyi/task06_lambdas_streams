package com.mushynskyi.menu;

public interface Printable {
  void print();
}
