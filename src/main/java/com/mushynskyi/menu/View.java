package com.mushynskyi.menu;

import com.mushynskyi.command.CommandManager;
import com.mushynskyi.stream.StreamManager;
import com.mushynskyi.taskone.implementation.ImplementationFunc;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Logger logger = LogManager.getLogger(View.class);
  private static Scanner input = new Scanner(System.in);

  public View() {
    menu = new LinkedHashMap<>();
    menu.put("1", "Press  1 - execute write command");
    menu.put("2", "Press  2 - execute write text");
    menu.put("3", "Press  3 - execute default tasks");
    menu.put("Q", "Press  Q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::writeCommand);
    methodsMenu.put("2", this::writeText);
    methodsMenu.put("3", this::defaultTasks);
  }

  private void printMenuAction() {
    logger.info("--------------MENU-----------\n");
    for (String str : menu.values()) {
      logger.info(str);
    }
  }

  public void showMenu() {
    String keyMenu ;
    do {
      printMenuAction();
      keyMenu = input.nextLine().toUpperCase();
      methodsMenu.get(keyMenu).print();
    }while (!keyMenu.equals("Q"));
  }

  private void writeCommand() {
    CommandManager commandManager = new CommandManager();
    String comand;
    String stringParam;
    comand = input.nextLine();
    stringParam = input.nextLine();
    commandManager.taskTwo(comand, stringParam);
  }

  private void writeText() {
    StreamManager streamManager = new StreamManager();
    String string;
    string = input.nextLine();
    streamManager.taskThree();
    streamManager.taskFour(string);
  }

  private void defaultTasks() {
    new ImplementationFunc().printTaskOne();
  }
}
